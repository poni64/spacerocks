{
    "id": "cef75e2b-7c7f-4977-a0f8-5951f51a2c18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6aae0846-f1bc-4fc7-9e10-66d8e90bcbd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef75e2b-7c7f-4977-a0f8-5951f51a2c18",
            "compositeImage": {
                "id": "fee84728-6536-4c5a-8600-27b562739e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aae0846-f1bc-4fc7-9e10-66d8e90bcbd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e0a2a6-bce1-4c69-b9ac-259ee65879f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aae0846-f1bc-4fc7-9e10-66d8e90bcbd9",
                    "LayerId": "c19e85eb-f0c3-4e3f-99dd-d79b6c35cc4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c19e85eb-f0c3-4e3f-99dd-d79b6c35cc4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cef75e2b-7c7f-4977-a0f8-5951f51a2c18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}