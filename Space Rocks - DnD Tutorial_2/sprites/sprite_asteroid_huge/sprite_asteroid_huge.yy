{
    "id": "f1ad90fe-95e1-4958-9ba6-69f8a5add04b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a06e47c1-7e71-4bfe-a2f3-fda8104b5c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1ad90fe-95e1-4958-9ba6-69f8a5add04b",
            "compositeImage": {
                "id": "30592d68-443e-461f-ba8f-5e5af8e8c2b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a06e47c1-7e71-4bfe-a2f3-fda8104b5c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef6e626-ce86-4ab7-b46c-0d33cdd8019e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a06e47c1-7e71-4bfe-a2f3-fda8104b5c16",
                    "LayerId": "913f68f7-de5c-4f18-ad53-db8ea6588758"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "913f68f7-de5c-4f18-ad53-db8ea6588758",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1ad90fe-95e1-4958-9ba6-69f8a5add04b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}