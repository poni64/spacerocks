{
    "id": "fe05a7f1-c322-4721-90d1-f30e41564028",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9323d07c-05f8-42b3-b00f-baa555e99500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe05a7f1-c322-4721-90d1-f30e41564028",
            "compositeImage": {
                "id": "4c23af7d-c8c6-46df-9eb1-6859f7f9e26d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9323d07c-05f8-42b3-b00f-baa555e99500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57ad8303-b675-47ad-a343-fa02741bc516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9323d07c-05f8-42b3-b00f-baa555e99500",
                    "LayerId": "d3a75653-db26-48a5-b42f-134b3144b2c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d3a75653-db26-48a5-b42f-134b3144b2c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe05a7f1-c322-4721-90d1-f30e41564028",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}