{
    "id": "f377f0b3-d6ed-41f8-977c-3811929becaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteriod_smol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b656e9a0-500e-4f04-a2f5-0e25e9d18f2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f377f0b3-d6ed-41f8-977c-3811929becaa",
            "compositeImage": {
                "id": "e81cf339-9ec7-4852-9974-64a8f4a2b42f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b656e9a0-500e-4f04-a2f5-0e25e9d18f2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c93753b1-c626-4770-8538-4dc9fc676c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b656e9a0-500e-4f04-a2f5-0e25e9d18f2d",
                    "LayerId": "67401817-7e95-4e19-808b-2e4d32f0e221"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "67401817-7e95-4e19-808b-2e4d32f0e221",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f377f0b3-d6ed-41f8-977c-3811929becaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}